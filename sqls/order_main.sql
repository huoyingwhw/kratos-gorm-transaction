CREATE TABLE `order_main`
(
    `id`          int unsigned NOT NULL AUTO_INCREMENT,
    `unique_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `stock`       int                                                           NOT NULL,
    `super_order` int unsigned NOT NULL DEFAULT '0' COMMENT '0-普通order;1-super order',
    `created_at`  datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`  datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `idx_unique_code` (`unique_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;