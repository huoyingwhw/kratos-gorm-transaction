-- 创建有 数组 类型字段的表
create table whw_ck_test_db.staff
(
    sid     UUID comment '员工id',
    s_name  String comment '员工姓名',
    age     UInt8 comment '员工年龄',
    hobbies Array(String) DEFAULT [] comment '爱好'
) ENGINE = MergeTree ORDER BY (sid,s_name) PRIMARY KEY (sid);

-- 往有嵌套类型字段的表中插入数据
insert into whw_ck_test_db.staff
VALUES (generateUUIDv4(), 'whw', 22, ['football', 'basketball']),
