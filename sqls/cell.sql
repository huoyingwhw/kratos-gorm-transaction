

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cell
-- ----------------------------
DROP TABLE IF EXISTS `cell`;
CREATE TABLE `cell` (
                        `id` int unsigned NOT NULL AUTO_INCREMENT,
                        `name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
                        `parent_id` int unsigned NOT NULL,
                        `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        PRIMARY KEY (`id`),
                        KEY `idx_parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of cell
-- ----------------------------
BEGIN;
INSERT INTO `cell` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES (1, 'cell1', 0, '2023-07-25 03:58:30', '2023-07-25 03:58:30');
INSERT INTO `cell` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES (2, 'cell11', 1, '2023-07-25 03:58:39', '2023-07-25 03:58:39');
INSERT INTO `cell` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES (3, 'cell12', 1, '2023-07-25 03:58:53', '2023-07-25 03:58:53');
INSERT INTO `cell` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES (4, 'cell13', 1, '2023-07-25 03:59:00', '2023-07-25 03:59:00');
INSERT INTO `cell` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES (5, 'cell131', 4, '2023-07-25 03:59:11', '2023-07-25 03:59:11');
INSERT INTO `cell` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES (6, 'cell132', 4, '2023-07-25 03:59:20', '2023-07-25 03:59:20');
INSERT INTO `cell` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES (7, 'cell1321', 6, '2023-07-25 03:59:36', '2023-07-25 03:59:36');
INSERT INTO `cell` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES (8, 'cell1322', 6, '2023-07-25 03:59:46', '2023-07-25 03:59:46');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
