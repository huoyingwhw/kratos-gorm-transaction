CREATE TABLE `order_statement`
(
    `id`            int unsigned NOT NULL AUTO_INCREMENT,
    `order_main_id` int          NOT NULL,
    `order_no`      varchar(255) NOT NULL DEFAULT '',
    `change_value`  int          NOT NULL,
    `is_delete`     tinyint      NOT NULL DEFAULT '0' COMMENT '0:未删除,1:已删除',
    `created_at`    datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`    datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;