package main

import (
	"github.com/go-kratos/kratos/contrib/registry/etcd/v2"
	"gorm_transaction/internal/boot"
	"gorm_transaction/internal/conf"

	"github.com/go-kratos/kratos/v2"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/transport/grpc"
	"github.com/go-kratos/kratos/v2/transport/http"

	_ "go.uber.org/automaxprocs"
)

func newApp(conf *conf.Server, logger log.Logger, gs *grpc.Server, hs *http.Server, registry *etcd.Registry) *kratos.App {
	return kratos.New(
		kratos.ID(conf.GetId()),
		kratos.Name(conf.GetName()), // service的名称改成从配置文件中配置
		kratos.Version(conf.GetVersion()),
		kratos.Metadata(map[string]string{}),
		kratos.Logger(logger),
		kratos.Server(
			gs,
			hs,
		),
		kratos.Registrar(registry),
	)
}

func main() {

	// 初始化配置
	bc := boot.NewBootConf().Run()
	// 初始化log
	logger := boot.NewBootLog(bc).Run()
	// 初始化trace
	boot.NewBootTrace(bc).Run()

	app, cleanup, err := wireApp(bc.Server, bc.Data, bc.Registry, logger)
	if err != nil {
		panic(err)
	}
	defer cleanup()

	// start and wait for stop signal
	if err := app.Run(); err != nil {
		panic(err)
	}
}
