在 Golang 中使用 GORM 操作 MySQL，并**在循环外开启事务，可以实现在循环内的某一个操作失败时回滚整个事务。其他操作也会被回滚，确保数据的一致性。**

下面是一个简单的示例代码，演示了如何使用 GORM 在循环外开启事务并进行相关操作：

```go
package main

import (
	"fmt"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// 定义模型
type User struct {
	ID   uint   `gorm:"primaryKey"`
	Name string `gorm:"unique"`
	Age  int
}

func main() {
	dsn := "user:password@tcp(localhost:3306)/database_name?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Failed to connect to database:", err)
	}

	// 自动迁移数据库表
	err = db.AutoMigrate(&User{})
	if err != nil {
		log.Fatal("Failed to auto migrate table:", err)
	}

	// 开始事务
	tx := db.Begin()
	if tx.Error != nil {
		log.Fatal("Failed to start transaction:", tx.Error)
	}

	// 定义一个操作失败的标志
	operationFailed := false

	// 模拟循环内的操作
	for i := 1; i <= 5; i++ {
		user := User{
			Name: fmt.Sprintf("User%d", i),
			Age:  20 + i,
		}

		// 创建用户记录
		if err := tx.Create(&user).Error; err != nil {
			operationFailed = true
			break // 如果有错误，立即跳出循环
		}
	}

	// 根据操作失败标志判断是否回滚事务
	if operationFailed {
		tx.Rollback()
		fmt.Println("Transaction rolled back due to operation failure.")
	} else {
		tx.Commit()
		fmt.Println("Transaction committed successfully.")
	}
}
```

请注意，在实际应用中，你可能需要根据你的业务逻辑和数据库模型进行适当的修改。此示例中的 `User` 模型是一个简单的示例，你可以根据你的需求进行更改。同时，也要确保 GORM 版本已经正确安装，代码中使用的是 `gorm.io/gorm` 和 `gorm.io/driver/mysql`。

在循环中，如果任意一次创建用户记录的操作失败（例如某个唯一约束冲突或其他错误），将设置 `operationFailed` 为 `true`，从而使得整个事务回滚。否则，循环结束后，会提交事务。