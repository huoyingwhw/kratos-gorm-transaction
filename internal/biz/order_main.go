package biz

import "time"

const (
	OrderMainTableName = "order_main"
)

type OrderMain struct {
	Id         uint32     `gorm:"column:id;primary_key" json:"id"`
	UniqueCode string     `gorm:"unique_code" json:"uniqueCode"` // 唯一标识
	Stock      int64      `gorm:"column:stock" json:"stock"`
	SuperOrder uint       `gorm:"column:super_order" json:"superOrder"`
	CreatedAt  *time.Time `gorm:"column:created_at;default:null" json:"createdAt"`
	UpdatedAt  *time.Time `gorm:"column:updated_at;default:null" json:"updatedAt"`
}

func (o *OrderMain) TableName() string {
	return OrderMainTableName
}
