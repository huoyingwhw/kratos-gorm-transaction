package data

import (
	"context"
	"github.com/go-kratos/kratos/v2/log"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"gorm_transaction/internal/biz"
)

type staffRepo struct {
	data *Data
	log  *log.Helper
}

func NewStaffRepo(data *Data, logger log.Logger) biz.StaffBizInterface {
	return &staffRepo{
		data: data,
		log:  log.NewHelper(logger),
	}
}

func (s *staffRepo) GetStaffsBySName(ctx context.Context, sName string) ([]*biz.Staff, error) {
	ctx, span := otel.Tracer("DATA").Start(ctx, "GetStaffsBySNameDATA", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	ret := make([]*biz.Staff, 0)

	errFind := s.data.CK.Table(biz.StaffTableName).WithContext(ctx).Where("s_name = ?", sName).Find(&ret).Error
	if errFind != nil {
		return nil, errFind
	}

	return ret, nil
}

func (s *staffRepo) CreateStaffs(ctx context.Context, staffs []*biz.Staff) error {
	ctx, span := otel.Tracer("DATA").Start(ctx, "CreateStaffsDATA", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	err := s.data.CK.Table(biz.StaffTableName).WithContext(ctx).Create(staffs).Error

	return err
}
