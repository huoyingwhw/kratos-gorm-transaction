package data

import (
	"github.com/go-kratos/kratos/v2/log"
	"gorm_transaction/internal/biz"
)

type amount1Repo struct {
	data *Data
	log  *log.Helper
}

func NewAmount1Repo(data *Data, logger log.Logger) biz.Amount1BizInterface {
	return &amount1Repo{
		data: data,
		log:  log.NewHelper(logger),
	}
}
