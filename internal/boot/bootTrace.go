package boot

import (
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/sdk/resource"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.21.0"
	"gorm_transaction/internal/conf"
	"os"
)

type BootTrace struct {
	conf *conf.Bootstrap
}

func (t *BootTrace) Run() {
	var tp *tracesdk.TracerProvider

	resourceRet, errRet := t.resource()
	if errRet != nil {
		log.Errorw("trace_resource_error", errRet)
		panic(errRet)
	}
	if t.conf.Trace.Enable {
		exporter, err := t.exporter(t.conf.Trace.Exporter) // Notice 配置的 jaeger
		if err != nil {
			log.Errorw("trace_exporter_error", err)
			panic(err)
		}
		tp = tracesdk.NewTracerProvider(
			tracesdk.WithBatcher(exporter),
			tracesdk.WithResource(resourceRet),
		)
	} else {
		tp = tracesdk.NewTracerProvider(
			tracesdk.WithResource(resourceRet),
		)
	}
	otel.SetTracerProvider(tp)

}

func (t *BootTrace) exporter(types string) (tracesdk.SpanExporter, error) {
	var exporterRet tracesdk.SpanExporter
	var err error

	switch types {
	case "stdout":
		exporterRet, err = stdouttrace.New(
			stdouttrace.WithPrettyPrint())
	case "file":
		var osFile *os.File
		osFile, err = os.Create(t.conf.Trace.TraceFilePath)
		exporterRet, err = stdouttrace.New(
			stdouttrace.WithWriter(osFile))
	case "jaeger":
		exporterRet, err = jaeger.New(
			jaeger.WithCollectorEndpoint(
				jaeger.WithEndpoint(t.conf.Trace.Endpoint),
			))
	}
	return exporterRet, err
}

func (t *BootTrace) resource() (*resource.Resource, error) {
	resourceRet, err := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String(t.conf.Server.Name),          //实例名称
			attribute.String("environment", t.conf.Server.Environment), // 相关环境
			attribute.String("ID", t.conf.Server.Version),              //版本
			attribute.String("token", t.conf.Trace.Token),              //token
		),
	)
	fmt.Println("errResource:>>>>> ", err)
	return resourceRet, err
}
