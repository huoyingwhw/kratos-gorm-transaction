package boot

import "gorm_transaction/internal/conf"

type Boot interface {
	Run()
	Setting()
}

func NewBootConf() *BootConf {
	return &BootConf{}
}

func NewBootLog(conf *conf.Bootstrap) *BootLog {
	return &BootLog{
		conf: conf,
	}
}

func NewBootTrace(conf *conf.Bootstrap) *BootTrace {
	return &BootTrace{
		conf: conf,
	}
}
