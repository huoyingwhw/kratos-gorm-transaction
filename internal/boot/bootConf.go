package boot

import (
	"flag"
	"github.com/go-kratos/kratos/v2/config"
	"github.com/go-kratos/kratos/v2/config/file"
	"github.com/google/uuid"
	"gorm_transaction/internal/conf"
)

var (
	flagConf string
)

func init() {
	// 获取命令行参数
	flag.StringVar(&flagConf, "conf", "../../configs", "config path, eg: -conf config.yaml")
	flag.Parse()
}

type BootConf struct{}

func (c *BootConf) Run() *conf.Bootstrap {
	// 获取指定路径 yaml文件 的配置
	configParams := config.New(
		config.WithSource(
			file.NewSource(flagConf),
		),
	)

	if errLoad := configParams.Load(); errLoad != nil {
		panic(errLoad)
	}

	var bc conf.Bootstrap
	if errScan := configParams.Scan(&bc); errScan != nil {
		panic(errScan)
	}

	serverId, _ := uuid.NewUUID()
	bc.Server.Id = serverId.String()

	return &bc
}
