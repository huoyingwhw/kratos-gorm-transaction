package service

import (
	"context"
	amount1_v1 "gorm_transaction/api/Amount1Server/v1"
)

// 返回 当前cell以及它所有的子cell(结构化数据)
func (a *Amount1Service) RecursionGetCellChildren(ctx context.Context, req *amount1_v1.RecursionGetCellDataRep) (*amount1_v1.RecursionGetCellChildrenDataLayerReply, error) {
	return a.cellBiz.RecursionGetCellChildren(ctx, req)
}

// 返回 当前cell以及它所有的父级cell(返回一个list)
func (a *Amount1Service) RecursionGetCellParents(ctx context.Context, req *amount1_v1.RecursionGetCellDataRep) (*amount1_v1.RecursionGetCellParentDataLayerReply, error) {
	return a.cellBiz.RecursionGetCellParents(ctx, req)
}
