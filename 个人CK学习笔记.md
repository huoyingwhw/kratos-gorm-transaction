## 🍎 《ClickHouse原理解析与应用实践》

一下内容主要摘录自《ClickHouse原理解析与应用实践》，还有一些是从网上查询资料摘录下来的。

### ✅ 系统表的使用

ClickHouse 的系统表为用户提供了关于数据库状态和性能的丰富信息。这些表位于 `system` 数据库中，可以帮助您监控数据库的健康状况、性能指标、查询统计等。下面列出了一些常用的系统表及其用途，以及一些示例 SQL 查询：

1. **system.tables**：
   - 描述：包含有关数据库中所有表的信息。
   - 常用查询：检索特定数据库中的所有表。
   - 示例 SQL：`SELECT * FROM system.tables WHERE database = 'your_database';`
2. **system.columns**：
   - 描述：提供有关所有表中列的信息。
   - 常用查询：查找特定表中的列及其类型。
   - 示例 SQL：`SELECT * FROM system.columns WHERE table = 'your_table';`
3. **system.parts**：
   - 描述：显示有关表分区的信息。
   - 常用查询：检查表的分区情况。
   - 示例 SQL：`SELECT * FROM system.parts WHERE table = 'your_table';`
4. **system.processes**：
   - 描述：显示当前正在执行的查询。
   - 常用查询：监控当前运行的查询。
   - 示例 SQL：`SELECT * FROM system.processes;`
5. **system.metrics**：
   - 描述：提供有关服务器性能的即时度量标准。
   - 常用查询：监控服务器的实时性能。
   - 示例 SQL：`SELECT * FROM system.metrics;`
6. **system.events**：
   - 描述：包含服务器操作的累积计数器。
   - 常用查询：了解服务器操作统计。
   - 示例 SQL：`SELECT * FROM system.events;`
7. **system.query_log**：
   - 描述：包含执行的查询日志。
   - 常用查询：分析查询历史和性能。
   - 示例 SQL：`SELECT * FROM system.query_log WHERE type = 1 ORDER BY event_time DESC LIMIT 10;`
8. **system.asynchronous_metrics**：
   - 描述：提供一些后台任务的异步度量。
   - 常用查询：了解后台任务的状态。
   - 示例 SQL：`SELECT * FROM system.asynchronous_metrics;`
9. **system.databases**：
   - 描述：展示当前实例中的所有数据库。
   - 常用查询：获取数据库列表。
   - 示例 SQL：`SELECT * FROM system.databases;`
10. **system.replicas**：

- 描述：对于使用复制的表，提供复制状态信息。
- 常用查询：监控复制表的状态。
- 示例 SQL：`SELECT * FROM system.replicas;`

这些只是 ClickHouse 系统表的一小部分示例。ClickHouse 提供了大量的系统表，每个都有其独特的用途和信息。了解和使用这些表可以帮助您更好地理解和优化您的 ClickHouse 实例。建议您查阅 ClickHouse 的官方文档，以获取更全面的信息和更详细的使用示例。

### ✅ 创建数据库语句

```sql
create database whw_ck_test_db2;
```

### ✅ 建表的时候加注释

```sql
-- 创建有 嵌套、数组、元组 类型字段的表
create table whw_ck_test_db.staff (
      sid UUID comment '员工id',
      s_name String comment '员工姓名',
      age UInt8 comment '员工年龄',
      dept Nested(
          id UInt8,
          dept_name String
          ) comment '部门信息',
      hobbies Array(String) DEFAULT [] comment '爱好',
      attributes Tuple(String,UInt64,DATETIME) comment '其他属性'
) ENGINE = MergeTree ORDER BY (sid,dept.id,dept.dept_name) PRIMARY KEY (sid);

-- 插入数据
-- 往有嵌套类型字段的表中插入数据
insert into whw_ck_test_db.staff VALUES (generateUUIDv4(), 'whw', 22, [10, 11], ['研发部','技术支持中心'],['football','basketball'],('ff',12,now())),
                                        (generateUUIDv4(), 'naruto', 24, [10, 11], ['研发部','技术支持中心'],['football'],('gg',10,now()));

-- 查询
select * from whw_ck_test_db.staff ARRAY JOIN dept where dept.dept_name = '研发部';
-- 结果
sid,s_name,age,`dept.id`,`dept.dept_name`,hobbide,attributes
06eb6f2a-27f4-4d84-a2f5-5f3fdefca090,naruto,24,10,研发部,['football'],"('gg', 10, '2023-12-09T08:57:04')"
83b75fbb-93ba-4a9f-ba24-a71ab632e347,whw,22,10,研发部,"['football', 'basketball']","('ff', 12, '2023-12-09T08:57:04')"

```

### ✅ ❗️关于建表时候的“排序键”

在ClickHouse中，**排序键**（**ORDER BY子句中指定的字段**）对于表的性能和查询优化非常关键。排序键的主要作用如下：

1. **数据物理排序**：ClickHouse在存储数据时会根据排序键对数据进行物理排序。这意味着具有相似键值的数据记录会存储在一起，从而优化查询效率，尤其是对于范围查询和顺序扫描。
2. **提高数据压缩效率**：由于相似的数据存储在一起，这有助于提高数据压缩效率，因为相似或重复的数据更容易被有效压缩。
3. **索引和数据检索**：排序键实际上充当了一种索引，帮助ClickHouse快速定位和检索数据。当执行查询时，ClickHouse可以利用排序键快速找到所需数据的位置，尤其是在进行范围查询或顺序查询时。
4. **数据分区效率**：在使用分区表时，排序键有助于每个分区内部的数据组织，使得对特定分区的查询更加高效。
5. **优化JOIN操作**：如果在JOIN操作中使用的是排序键上的列，那么这样的JOIN操作通常会更高效，因为数据已经根据这些键排序。
6. **数据去重**：在某些MergeTree表引擎变体中（如ReplacingMergeTree、SummingMergeTree等），排序键用于数据去重和聚合。
7. **数据抽样**：排序键还可以用于数据抽样，允许你有效地从大表中获取代表性样本。

**❗️选择合适的排序键非常重要，因为它直接影响数据存储结构和查询性能。理想情况下，排序键应该是你最常用于查询的列，尤其是那些用于WHERE子句中的范围或等值条件的列。**

**❗️然而，过多的排序键列会增加数据存储的开销，并可能降低写入性能。因此，设计排序键时需要在查询优化和写入/存储效率之间找到平衡。**

### ✅ ❗️CK“分析查询性能”常用的方法

ClickHouse 提供了几种方法来分析查询的执行性能，类似于 MySQL 中的 `EXPLAIN` 语句。以下是在 ClickHouse 中进行查询性能分析的一些常用方法：

1. **EXPLAIN**:
   - ClickHouse 也有 `EXPLAIN` 语句，但它的功能相对较基础。它可以用来显示查询的执行计划。
   - 用法示例：`EXPLAIN SELECT * FROM table;`
2. **EXPLAIN ESTIMATES**:
   - 这个命令提供了关于查询中各个阶段将要处理的行数和字节数的估计。
   - 用法示例：`EXPLAIN ESTIMATES SELECT * FROM table;`
3. **系统表**:
   - ClickHouse 提供了一些系统表，如 `system.query_log` 和 `system.query_thread_log`，可以用来分析查询的执行详情。
   - 通过查询这些表，可以获得关于执行时间、读取的行数、字节数等详细信息。
4. **PROFILE**:
   - `SET profile = 1;` 命令可以用来开启更详细的查询分析。
   - 执行此命令后，查询的结果将包含一个额外的 `ProfileEvents` 列，其中包含了各种性能指标，如 CPU 使用时间、内存使用量等。
5. **TRACE**:
   - `SET send_logs_level = 'trace';` 命令可以用来在查询执行时收集更多的日志信息。
   - 这可以帮助您了解查询执行的更多细节。
6. **FORMAT JSON**:
   - 将查询结果以 JSON 格式输出，可以获得关于查询执行的额外信息。
   - 用法示例：`SELECT * FROM table FORMAT JSON;`

使用这些工具，您可以深入了解 ClickHouse 中的查询执行情况，从而对查询进行优化和调整。不过，需要注意的是，这些工具在不同版本的 ClickHouse 中可能有所不同，因此最好参考您所使用的 ClickHouse 版本的官方文档。

### ✅ ❗️基础类型

#### ✨数值类型计算的精度问题

❗️**《ClickHouse原理解析与应用实践》—— 4.11章**

#### ✨ UUID类型 & 简单的测试语句

```sql
-- 创建数据库必须指定 ENGINE 与 PRIMARY KEY
create TABLE whw_ck_test_db.table1 (
   tid String,
   c1 UUID,
   c2 String
) ENGINE = MergeTree ORDER BY(tid,c2) PRIMARY KEY (tid) ;

-- ❗️当使用多个列字段排序时，以ORDER BY（CounterID,EventDate）为例，在单个数据片段内，数据首先会以CounterID排序，相同CounterID的数据再按EventDate排序。

-- 第一行UUID有值
INSERT INTO whw_ck_test_db.table1 VALUES('t1', generateUUIDv4(),'c11');
-- 第二行UUID没有值
INSERT INTO whw_ck_test_db.table1(tid,c2) VALUES('t2','c22');

-- 查看结果：UUID类型不指定的话 默认是0值:
select * from whw_ck_test_db.table1;
-- 结果: 
tid,c1,c2
t1,04fa0b3f-0dc0-4b6f-a86a-ae80267c1402,c11
t2,00000000-0000-0000-0000-000000000000,c22


```

### ✅ 复合类型❗️

#### ✨复合类型综合的例子

```sql
-- 创建有 嵌套、数组、元组 类型字段的表
create table whw_ck_test_db.staff (
      sid UUID comment '员工id',
      s_name String comment '员工姓名',
      age UInt8 comment '员工年龄',
      dept Nested(
          id UInt8,
          dept_name String
          ) comment '部门信息',
      hobbies Array(String) DEFAULT [] comment '爱好',
      attributes Tuple(String,UInt64,DATETIME) comment '其他属性'
) ENGINE = MergeTree ORDER BY (sid,dept.id,dept.dept_name) PRIMARY KEY (sid);

-- 插入数据
-- 往有嵌套类型字段的表中插入数据
insert into whw_ck_test_db.staff VALUES (generateUUIDv4(), 'whw', 22, [10, 11], ['研发部','技术支持中心'],['football','basketball'],('ff',12,now())),
                                        (generateUUIDv4(), 'naruto', 24, [10, 11], ['研发部','技术支持中心'],['football'],('gg',10,now()));

-- 查询
select * from whw_ck_test_db.staff ARRAY JOIN dept where dept.dept_name = '研发部';
-- 结果
sid,s_name,age,`dept.id`,`dept.dept_name`,hobbide,attributes
06eb6f2a-27f4-4d84-a2f5-5f3fdefca090,naruto,24,10,研发部,['football'],"('gg', 10, '2023-12-09T08:57:04')"
83b75fbb-93ba-4a9f-ba24-a71ab632e347,whw,22,10,研发部,"['football', 'basketball']","('ff', 12, '2023-12-09T08:57:04')"

```

#### ✨🍌复合类型综合——GORM数据定义+操作 

**这个项目中有例子：**[huoyingwhw/kratosGormTransaction](https://gitee.com/huoyingwhw/kratos-gorm-transaction)

#### ✨数组类型Array

**《ClickHouse原理解析与应用实践》—— 4.12章**

#### ✨元组类型Tuple

**《ClickHouse原理解析与应用实践》—— 4.12章**

#### ✨枚举类型Enum

**《ClickHouse原理解析与应用实践》—— 4.12章**

#### ✨ 嵌套类型Nested

**《ClickHouse原理解析与应用实践》—— 4.12章**

**嵌套类型，顾名思义是一种嵌套表结构。一张数据表，可以定义任意多个嵌套类型字段。**

**❗️但每个字段的嵌套层级只支持一级，即嵌套表内不能继续使用嵌套类型。**

**对于简单场景的层级关系或关联关系，使用嵌套类型也是一种不错的选择。例如，下面的nested_test是一张模拟的员工表，它的所属部门字段就使用了嵌套类型：**

```sql
-- 创建有嵌套类型字段的表
-- ❗️可以设置 嵌套字段里面的具体字段为 排序键！！！
create table whw_ck_test_db.staff (
      sid UUID,
      s_name String,
      age UInt8,
      dept Nested(
          id UInt8,
          dept_name String
          )
) ENGINE = MergeTree ORDER BY (sid,dept.id,dept.dept_name) PRIMARY KEY (sid);

-- Insert 数据的时候 期望写入的是一个Array数组类型
-- 嵌套类型本质是一种多维数组的结构。嵌套表中的每个字段都是一个数组，并且行与行之间数组的长度无须对齐
insert into whw_ck_test_db.staff VALUES (generateUUIDv4(), 'whw', 22, [10, 11], ['研发部','技术支持中心']),(generateUUIDv4(),'naruto', 23,[12, 13], ['销售部','运营部']);

-- 注意嵌套类型的数据在数据库中存储的格式如下：
sid,s_name,age,dept.id,dept.dept_name
f3c96f92-7077-40c9-9f57-7831a378688f,whw,22,"[10, 11]","['研发部', '技术支持中心']"
3dfcc750-d710-46c3-b3ce-aa1d80e344e2,naruto,23,"[12, 13]","['销售部', '运营部']"

-- 查询结果中带嵌套类型字段
select s_name, age, dept.dept_name from whw_ck_test_db.staff where s_name = 'whw';

s_name,age,dept.dept_name
whw,22,"['研发部', '技术支持中心']"

-- ❗️❗️❗️以嵌套类型字段为条件查询: ARRAY JOIN
-- 查询部门是“研发部”的所有人的记录
select * from whw_ck_test_db.staff ARRAY JOIN dept where dept.dept_name = '研发部';

sid,s_name,age,`dept.id`,`dept.dept_name`
f3c96f92-7077-40c9-9f57-7831a378688f,whw,22,10,研发部

/*
  这个查询做了以下操作：
  从staff表中选择所有列。
  使用ARRAY JOIN将dept嵌套结构展开为多行。
  在WHERE子句中筛选出dept_name为“研发部”的行。
  通过这种方式，你可以得到所有dept_name字段值为“研发部”的记录。
*/
```



### ✅ ❗️定义数据表——4.2章

**《ClickHouse原理解析与应用实践》—— 4.2章**

#### ✨❗️数据库



#### ✨❗️数据表



#### ✨❗️默认值表达式



#### ✨❗️临时表



#### ✨❗️分区表



#### ✨❗️视图



### ✅ ❗️数据表的基本操作

#### ✨ 加一个新字段

```sql
-- 在age字段后面加一个新字段 birthday，DATE类型的 默认值如下
ALTER TABLE whw_ck_test_db.staff ADD COLUMN birthday DATE DEFAULT toDate('0000-00-00') AFTER age;

-- 加完新字段以后 表里的数据如下:
c92e15fb-34c8-4691-84f6-54248e4ac5f7,whw,22,1970-01-01,"[10, 11]","['研发部', '技术支持中心']"
395be9a9-9933-4ab9-8c12-e27be6455b80,naruto,23,1970-01-01,"[12, 13]","['销售部', '运营部']"

```

#### ✨ 修改字段的数据类型或默认值

❗️修改某个字段的数据类型，实质上会调用相应的toType转型方法。如果当前的类型与期望的类型不能兼容，则修改操作将会失败。例如，将String类型的IP字段修改为IPv4类型是可行的。**而尝试将String类型转为UInt类型就会出现错误！**

```sql
-- 先新增一个 score字段 decimal(3,1) 类型的
ALTER TABLE whw_ck_test_db.staff ADD COLUMN score decimal(3,1) DEFAULT 0 AFTER age;
-- 修改成 UInt32类型的
ALTER TABLE whw_ck_test_db.staff MODIFY COLUMN score UInt32 DEFAULT 0;

-- 修改默认值
ALTER TABLE whw_ck_test_db.staff MODIFY COLUMN score UInt32 DEFAULT 60;

```

#### ✨ 修改备注

```sql
-- 为字段添加备注
ALTER TABLE whw_ck_test_db.staff COMMENT COLUMN sid 'uuid主键';
ALTER TABLE whw_ck_test_db.staff COMMENT COLUMN s_name '学生姓名';
ALTER TABLE whw_ck_test_db.staff COMMENT COLUMN age '学生年龄';

-- 或者在建表时候就就可以给字段添加备注
create table staff
(
    sid      UUID comment 'uuid主键',
    s_name   String comment '学生姓名',
    age      UInt8 comment '学生年龄',
    score    UInt32 default 60,
    birthday Date   default toDate('0000-00-00'),
    `dept.id` Array(UInt8),
    `dept.dept_name` Array(String)
)
    engine = MergeTree PRIMARY KEY sid
        ORDER BY (sid, dept.id, dept.dept_name)
        SETTINGS index_granularity = 8192;

```

#### ✨ 删除已有字段

```sql
-- 删除已有字段
ALTER TABLE whw_ck_test_db.staff DROP COLUMN birthday;
```

#### ✨ 移动数据表

（略）见书中讲解 《ClickHouse原理解析与应用实践》——4.3.5章

#### ✨ 清空数据表

假设需要将表内的数据全部清空，而不是直接删除这张表，则可以使用TRUNCATE语句，它的完整语法如下所示：

```sql
truncate table whw_ck_test_db.staff;
```

### ✅ ❗️数据的删除与修改

ClickHouse提供了DELETE和UPDATE的能力，这类操作被称为Mutation查询，它可以看作ALTER语句的变种。

虽然Mutation能最终实现修改和删除，但不能完全以通常意义上的UPDATE和DELETE来理解，我们必须清醒地认识到它的不同：

- 首先，Mutation语句是一种“很重”的操作，更适用于批量数据的修改和删除；
- 其次，它不支持事务，一旦语句被提交执行，就会立刻对现有数据产生影响，无法回滚；
- 最后，Mutation语句的执行是一个**异步的后台过程**，语句被提交之后就会立即返回！所以这并不代表具体逻辑已经执行完毕，它的具体执行进度需要通过system.mutations系统表查询。

DELETE语句的完整语法如下所示：

```sql
ALTER TABLE [db_name.]table_name DELETE WHERE filter_expr
```

数据删除的范围由WHERE查询子句决定。例如，执行下面语句可以删除partition_v2表内所有ID等于A003的数据：

```sql
ALTER TABLE partition_v2 DELETE WHERE ID = 'A003'
```

**❗️由于演示的数据很少，DELETE操作给人的感觉和常用的OLTP数据库无异。但是我们心中应该要明白这是一个异步的后台执行动作。**

**❗️❗️❗️《ClickHouse原理解析与应用实践》中4.7小节详细介绍了删除操作的内部原理！！认真研究下～**

（略）

```sql
-- 删除数据
ALTER TABLE whw_ck_test_db.staff DELETE where s_name = 'whw';

-- 修改数据❗️
ALTER TABLE whw_ck_test_db.staff UPDATE s_name='whw2' where s_name = 'whw';
-- 修改数据——改多个字段
ALTER TABLE whw_ck_test_db.staff UPDATE s_name='whw3',age=18 where s_name = 'whw2';
```

### ✅ ❗️❗️数据查询❗️❗️

如在绝大部分场景中，都应该避免使用`SELECT *`形式来查询数据，因为通配符*对于采用列式存储的ClickHouse而言没有任何好处。假如面对一张拥有数百个列字段的数据表，下面这两条SELECT语句的性能可能会相差100倍之多！

```sql
-- 使用通配符*与按列按需查询相比，性能可能相差100倍
SELECT * FROM datasets.hits_v1;

SELECT WatchID FROM datasets.hits_v1;

```

ClickHouse对于SQL语句的解析是大小写敏感的，这意味着SELECT a和SELECT A表示的语义是不相同的。ClickHouse目前支持的查询子句如下所示：

```sql
-- 按照优先级顺序
[WITH expr |(subquery)]

SELECT [DISTINCT] expr

[FROM [db.]table | (subquery) | table_function] [FINAL]

[SAMPLE expr]

[[LEFT] ARRAY JOIN]

[GLOBAL] [ALL|ANY|ASOF] [INNER | CROSS | [LEFT|RIGHT|FULL [OUTER]] ] JOIN
(subquery)|table ON|USING columns_list

[PREWHERE expr]

[WHERE expr]

[GROUP BY expr] [WITH ROLLUP|CUBE|TOTALS]

[HAVING expr]

[ORDER BY expr]

[LIMIT [n[,m]]
 
[UNION ALL]
 
[INTO OUTFILE filename]
 
[FORMAT format]
 
[LIMIT [offset] n BY columns]

```

其中，方括号包裹的查询子句表示其为可选项，所以只有SELECT子句是必须的。

而ClickHouse对于查询语法的解析也大致是按照上面各个子句排列的顺序进行的。

在本章后续会正视ClickHouse的本地查询部分，并大致依照各子句的解析顺序系统性地介绍它们的使用方法，而分布式查询部分则留待第10章介绍。

#### ✨WITH子句

**《ClickHouse原理解析与应用实践》—— 9.1章。**

- ❗️在WITH中使用**子查询**时有一点需要特别注意，该查询语句只能返回一行数据，如果结果集的数据大于一行则会抛出异常。

#### ✨FROM子句

...

#### ✨SAMPLE子句(采样)

SAMPLE子句能够实现数据采样的功能，**使查询仅返回采样数据**而不是全部数据，从而有效减少查询负载。

**SAMPLE子句的采样机制是一种幂等设计，也就是说在数据不发生变化的情况下，使用相同的采样规则总是能够返回相同的数据，所以这项特性非常适合在那些可以接受近似查询结果的场合使用。**

例如在数据量十分巨大的情况下，对查询时效性的要求大于准确性时就可以尝试使用SAMPLE子句。

**SAMPLE子句只能用于MergeTree系列引擎的数据表，并且要求在CREATE TABLE时声明SAMPLE BY抽样表达式**，例如下面的语句：

```sql
CREATE TABLE whw_ck_test_db.hist_v1 (
    counterId UInt64 comment '',
    eventDate DATE comment '',
    userId UInt64 comment ''
) ENGINE = MergeTree
PARTITION BY toYYYYMM(eventDate)
ORDER BY (counterId, intHash32(userId))
-- Sample Key 生命的表达式必须也包含在主键的声明中
-- SAMPLE BY表示hits_v1内的数据，可以按照intHash32(UserID)分布后的结果采样查询。
SAMPLE BY intHash32(userId);
```

❗️在声明Sample Key的时候有两点需要注意：

- SAMPLE BY所声明的表达式必须同时包含在主键的声明内
- **Sample Key必须是Int类型**，如若不是，ClickHouse在进行CREATE TABLE操作时也不会报错，但在数据查询时会得到如下类似异常

```sql
Invalid sampling column type in storage parameters: Float32. Must be unsigned integer type.
```

SAMPLE子句目前支持如下3种用法（略）。

**《ClickHouse原理解析与应用实践》—— 9.3章。**

**1.SAMPLE factor**（略）

**2.SAMPLE rows**（略）

**3.SAMPLE factor OFFSET n**（略）

#### ✨ARRAY JOIN子句--数组类型举例❗️

**ARRAY JOIN子句允许在数据表的内部，与`数组`或`嵌套`类型的字段进行JOIN操作，从而将一行数组展开为多行。**

**《ClickHouse原理解析与应用实践》—— 9.4章。**

ARRAY JOIN子句允许在数据表的内部，与数组或嵌套类型的字段进行JOIN操作，从而将一行数组展开为多行。接下来让我们看看它的基础用法。首先新建一张包含Array数组字段的测试表：

```sql
create table whw_ck_test_db.query_v1
(
    title String,
    value Array(UInt8)
) ENGINE = Log;
```

接着写入测试数据，注意最后一行数据的数组为空：

```sql
insert into whw_ck_test_db.query_v1 VALUES ('food', [1,2,3]), ('fruit', [3,4]), ('meat', []);

select * from whw_ck_test_db.query_v1;
-- 结果
title,value
food,"[1, 2, 3]"
fruit,"[3, 4]"
meat,[]

```

**在一条SELECT语句中，只能存在一个ARRAY JOIN（使用子查询除外）。目前支持INNER和LEFT两种JOIN策略：**

**1、INNER ARRAY JOIN —— 排除空数组**

ARRAY JOIN在默认情况下使用的是INNER JOIN策略，例如下面的语句：

```sql
select title, value from whw_ck_test_db.query_v1 ARRAY JOIN value;

-- 结果
title,value
food,1
food,2
food,3
fruit,3
fruit,4
```

从查询结果可以发现，最终的数据基于value数组被展开成了多行，**并且排除掉了空数组**。

在使用ARRAY JOIN时，如果为原有的数组字段添加一个别名，则能够访问展开前的数组字段，例如：

```sql
select title, value, v from whw_ck_test_db.query_v1 ARRAY JOIN value as v;

-- 结果
title,value,v
food,"[1, 2, 3]",1
food,"[1, 2, 3]",2
food,"[1, 2, 3]",3
fruit,"[3, 4]",3
fruit,"[3, 4]",4
```

**2、LEFT ARRAY JOIN**

ARRAY JOIN子句支持LEFT连接策略，例如执行下面的语句：

```sql
SELECT title,value,v FROM whw_ck_test_db.query_v1 LEFT ARRAY JOIN value AS v;

-- 结果: 在改为LEFT连接查询后，可以发现，在INNER JOIN中被排除掉的空数组出现在了返回的结果集中。
title,value,v
food,"[1, 2, 3]",1
food,"[1, 2, 3]",2
food,"[1, 2, 3]",3
fruit,"[3, 4]",3
fruit,"[3, 4]",4
meat,[],0
```

**当同时对多个数组字段进行ARRAY JOIN操作时，查询的计算逻辑是按行合并而不是产生笛卡儿积，例如下面的语句：**

```sql
-- ARRAY JOIN多个数组时，是合并，不是笛卡儿积
select title,value,v, arrayMap(x -> x*2,value) as mapv, v_1 from whw_ck_test_db.query_v1
LEFT ARRAY JOIN value AS v, mapv as v_1;

-- 结果
title,value,v,mapv,v_1
food,"[1, 2, 3]",1,"[2, 4, 6]",2
food,"[1, 2, 3]",2,"[2, 4, 6]",4
food,"[1, 2, 3]",3,"[2, 4, 6]",6
fruit,"[3, 4]",3,"[6, 8]",6
fruit,"[3, 4]",4,"[6, 8]",8
meat,[],0,[],0
```

#### ✨嵌套类型使用ARRAY JOIN子句

**在前面介绍数据定义时曾介绍过，嵌套数据类型的本质是数组，所以ARRAY JOIN也支持嵌套数据类型。**

接下来继续用一组示例说明。首先新建一张包含嵌套类型的测试表：

```sql
create table whw_ck_test_db.query_v2
(
    title String,
    nest Nested(
        v1 UInt32,
        v2 UInt64
    )
) ENGINE = Log;
```

**接着写入测试数据，在写入嵌套数据类型时，记得同一行数据中各个数组的长度需要对齐，而对多行数据之间的数组长度没有限制：**

```sql
INSERT INTO whw_ck_test_db.query_v2 VALUES
('food', [1,2,3], [10,20,30]),('fruit', [4,5],[40,50]),('meat', [], []);

-- 结果
title,`nest.v1`,`nest.v2`
food,"[1, 2, 3]","[10, 20, 30]"
fruit,"[4, 5]","[40, 50]"
meat,[],[]

```

**对嵌套类型数据的访问，ARRAY JOIN既可以直接使用字段列名：**

```sql
-- 带条件的
select title, nest.v1, nest.v2 from whw_ck_test_db.query_v2 ARRAY JOIN nest where nest.v2 > 10;
-- 结果
title,nest.v1,nest.v2
food,2,20
food,3,30
fruit,4,40
fruit,5,50

-- 使用点访问符的形式1：
select title, nest.v1, nest.v2 from whw_ck_test_db.query_v2 ARRAY JOIN nest.v1,nest.v2 where nest.v2 > 10;
-- 结果
title,nest.v1,nest.v2
food,2,20
food,3,30
fruit,4,40
fruit,5,50

-- 使用点访问符的形式2 —— 支持部分嵌套字段：在这种情形下，只有被ARRAY JOIN的数组才会展开
select title, nest.v1, nest.v2 from whw_ck_test_db.query_v2 ARRAY JOIN nest.v2 where nest.v2 > 10;
-- 结果
title,nest.v1,nest.v2
food,"[1, 2, 3]",20
food,"[1, 2, 3]",30
fruit,"[4, 5]",40
fruit,"[4, 5]",50

-- 在查询嵌套类型时也能够通过别名的形式访问原始数组：
select title, nest.v1, nest.v2, n.v1, n.v2 from whw_ck_test_db.query_v2 ARRAY JOIN nest as n;
-- 结果
title,nest.v1,nest.v2,n.v1,n.v2
food,"[1, 2, 3]","[10, 20, 30]",1,10
food,"[1, 2, 3]","[10, 20, 30]",2,20
food,"[1, 2, 3]","[10, 20, 30]",3,30
fruit,"[4, 5]","[40, 50]",4,40
fruit,"[4, 5]","[40, 50]",5,50


```

#### ✨JOIN子句

**《ClickHouse原理解析与应用实践》—— 9.5章。**

#### ✨WHERE与PREWHERE子句

**《ClickHouse原理解析与应用实践》—— 9.6章。**

#### ✨GROUP BY子句

GROUP BY又称聚合查询，是最常用的子句之一，它是让ClickHouse最凸显卓越性能的地方。

在GROUP BY后声明的表达式，通常称为聚合键或者Key，数据会按照聚合键进行聚合。

在ClickHouse的聚合查询中，SELECT可以声明聚合函数和列字段，如果SELECT后只声明了聚合函数，则可以省略GROUP BY关键字：

```sql
-- 如果只有聚合函数，可以省略GROUP BY
SELECT SUM(data_compressed_bytes) AS compressed,
SUM(data_uncompressed_bytes) AS uncompressed
FROM system.parts
```

如若声明了列字段，则只能使用聚合键包含的字段，否则会报错：

```sql
-- 除了聚合函数外，只能使用聚合key中包含的table字段
SELECT table,COUNT() FROM system.parts GROUP BY table

-- 使用聚合key中未声明的rows字段，则会报错
SELECT table,COUNT(),rows FROM system.parts GROUP BY table
```

但是在某些场合下，可以借助any、max和min等聚合函数访问聚合键之外的列字段：

```sql
SELECT table,COUNT(),any(rows) FROM system.parts GROUP BY table

┌─table────┬─COUNT()─┬─any(rows)─┐
│ partition_v1 │ 1 │ 4 │
│ agg_merge2 │ 1 │ 1 │
│ hits_v1 │ 2 │ 8873898 │

```

当聚合查询内的数据存在NULL值时，ClickHouse会将NULL作为NULL=NULL的特定值处理，例如：

```sql
SELECT arrayJoin([1, 2, 3,null,null,null]) AS v GROUP BY v
-- 结果
v
1
2
3
NULL
```

可以看到所有的NULL值都被聚合到了NULL分组。

除了上述特性之外，聚合查询目前还能配合**WITH ROLLUP**、**WITH CUBE**和**WITH TOTALS**三种修饰符获取额外的汇总信息。

**《ClickHouse原理解析与应用实践》—— 9.7.1章～9.7.3章**

（略）

#### ✨HAVING子句

**HAVING子句需要与GROUP BY同时出现，不能单独使用。它能够在聚合计算之后实现二次过滤数据。**

**《ClickHouse原理解析与应用实践》—— 9.8章**

#### ✨ORDER BY子句

- ORDER BY子句通过声明排序键来指定查询数据返回时的顺序。

- 通过先前的介绍大家知道，在MergeTree表引擎中也有ORDER BY参数用于指定排序键，那么这两者有何不同呢？

- 在MergeTree中指定ORDER BY后，数据在各个分区内会按照其定义的规则排序，这是一种分区内的局部排序。

- **如果在查询时数据跨越了多个分区，则它们的返回顺序是无法预知的**，每一次查询返回的顺序都可能不同。在这种情形下，如果需要数据总是能够按照期望的顺序返回，就需要借助ORDER BY子句来指定全局顺序。

ORDER BY在使用时可以定义多个排序键，每个排序键后需紧跟ASC（升序）或DESC（降序）来确定排列顺序。如若不写，则默认为ASC（升序）。例如下面的两条语句即是等价的：

```sql
-- 按照v1升序、v2降序排序
SELECT arrayJoin([1,2,3]) as v1 , arrayJoin([4,5,6]) as v2 ORDER BY v1 ASC, v2 DESC 

SELECT arrayJoin([1,2,3]) as v1 , arrayJoin([4,5,6]) as v2 ORDER BY v1, v2 DESC

-- 结果: 数据首先会按照v1升序，接着再按照v2降序。
v1,v2
1,6
1,5
1,4
2,6
2,5
2,4
3,6
3,5
3,4
```

对于数据中NULL值的排序，目前ClickHouse拥有**NULL值最后**和**NULL值优先**两种策略，可以通过NULLS修饰符进行设置：

**1、NULLS LAST**

NULL值排在最后，这也是默认行为，修饰符可以省略。在这种情形下，数据的排列顺序为其他值（value）→NaN→NULL。

```sql
-- 顺序是value -> NaN -> NULL
WITH arrayJoin([30,null,60.5,0/0,1/0,-1/0,30,null,0/0]) AS v1
select v1 ORDER BY v1 DESC NULLS LAST;

-- 结果
v1
Infinity
60.5
30
30
-Infinity
NaN
NaN
<null>
<null>

```

**2、NULLS FIRST**

NULL值排在最前，在这种情形下，数据的排列顺序为NULL→NaN→其他值（value）：

```sql
WITH arrayJoin([30,null,60.5,0/0,1/0,-1/0,30,null,0/0]) AS v1
select v1 order by v1 DESC NULLS FIRST;

-- 结果
v1
<null>
<null>
NaN
NaN
Infinity
60.5
30
30
-Infinity

```

从上述的两组测试中不难发现，对于NaN而言，它总是紧跟在NULL的身边。在使用NULLS LAST策略时，NaN好像比所有非NULL值都小；而在使用NULLS FIRST时，NaN又好像比所有非NULL值都大。

#### ✨LIMIT BY子句(TOP N)

LIMIT BY子句和大家常见的LIMIT所有不同，**它运行于ORDER BY之后和LIMIT之前**，能够按照指定分组，最多返回前n行数据（如果数据少于n行，则按实际数量返回），常用于TOP N的查询场景。

LIMIT BY的常规语法如下：

```sql
LIMIT n BY express
```

**❗️例如执行下面的语句后便能够在基于数据库和数据表分组的情况下，查询返回数据占磁盘空间最大的前3张表：**

```sql
-- 1、查所有表的占用空间
SELECT database,table,max(bytes_on_disk) AS bytes FROM system.parts GROUP BY
database, table ORDER BY database, bytes DESC;
-- 结果
database,table,bytes
system,trace_log,6686457
system,asynchronous_metric_log,4198299
system,metric_log,3218386
system,query_log,389413
system,part_log,4641
whw_ck_test_db,staff,523

-- 2、查询TOP3: 注意 LIMIT 2
SELECT database,table,max(bytes_on_disk) AS bytes FROM system.parts GROUP BY
database, table ORDER BY database, bytes DESC LIMIT 2 BY database;

```

**❗️声明多个表达式需使用逗号分隔，例如下面的语句能够得到每张数据表所定义的字段中，使用最频繁的前5种数据类型：**

```sql
select database,table,type,COUNT(name) AS col_count FROM system.columns GROUP BY
database, table, type ORDER BY col_count DESC LIMIT 5 BY database, table;

-- 结果
database,table,type,col_count
system,metric_log,UInt64,413
system,metric_log,Int64,110
system,projection_parts_columns,UInt64,22
information_schema,columns,String,18
INFORMATION_SCHEMA,COLUMNS,Nullable(String),18
system,quota_usage,Nullable(UInt64),18
INFORMATION_SCHEMA,COLUMNS,String,18
information_schema,columns,Nullable(String),18
-- ... 剩下的记录省略

```

**❗️除了常规语法以外，LIMIT BY也支持跳过OFFSET偏移量获取数据，具体语法如下：**

```sql
LIMIT n OFFSET y BY express 

-- 简写
LIMIT y,n BY express
```

例如在执行下面的语句时，查询会从跳过1行数据的位置开始：

```sql
SELECT database,table,MAX(bytes_on_disk) AS bytes FROM system.parts GROUP BY
database,table ORDER BY bytes DESC LIMIT 3 OFFSET 1 BY database;
```

使用简写形式也能够得到相同效果：

```sql
SELECT database,table,MAX(bytes_on_disk) AS bytes FROM system.parts GROUP BY
database,table ORDER BY bytes DESC LIMIT 1,3 BY database
```

#### ✨LIMIT子句

LIMIT子句用于返回指定的前n行数据，常用于分页场景，它的三种语法形式如下所示：

```sql
LIMIT n
LIMIT n OFFSET m
LIMIT m，n
```

**《ClickHouse原理解析与应用实践》—— 9.11章**

#### ✨SELECT子句

**《ClickHouse原理解析与应用实践》—— 9.12章**

SELECT子句决定了一次查询语句最终返回哪些列字段或表达式。

与直观的感受不同，虽然SELECT位于SQL语句的起始位置，但它却是在上述一众子句之后执行的。

在其他子句执行之后，SELECT会将选取的字段或表达式作用于每行数据之上。

如果使用`*`通配符，则会返回数据表的所有字段。**正如本章开篇所言，在大多数情况下都不建议这么做，因为对于一款列式存储的数据库而言，这绝对是劣势而不是优势。**

**❗️❗️❗️在选择列字段时，ClickHouse还为`特定场景`提供了一种基于正则查询的形式。例如执行下面的语句后，查询会返回名称以字母n开头和包含字母p的`列字段`：**

```sql
SELECT COLUMNS('^n'), COLUMNS('p') FROM system.databases;
-- 查询出来的 “列字段” 包含的规则：以“n开头”、或者“列中包含p字母的”：name、data_path、metadata_path
name,data_path,metadata_path
INFORMATION_SCHEMA,/var/lib/clickhouse/,""

default,/var/lib/clickhouse/store/,/var/lib/clickhouse/store/c38/c388fb9b-e10a-4a7d-8c70-8074d7880f8a/
information_schema,/var/lib/clickhouse/,""

system,/var/lib/clickhouse/store/,/var/lib/clickhouse/store/a8e/a8ebbf88-e681-41f2-b809-6fab62b38083/
whw_ck_test_db,/var/lib/clickhouse/store/,/var/lib/clickhouse/store/696/696442d8-333a-4c05-b8b0-074722e0df90/

```

#### ✨DISTINCT子句

**《ClickHouse原理解析与应用实践》—— 9.13章**

#### ✨UNION ALL子句

**《ClickHouse原理解析与应用实践》—— 9.14章**

#### ✨🍌查看SQL执行计划❗️

**《ClickHouse原理解析与应用实践》—— 9.15章 —— 书中介绍了另外一种方式，实际上现在版本的CK已经支持EXPLAIN语句了。**

ClickHouse 提供了几种方法来分析查询的执行性能，类似于 MySQL 中的 `EXPLAIN` 语句。以下是在 ClickHouse 中进行查询性能分析的一些常用方法：

1. **EXPLAIN**:
   - ClickHouse 也有 `EXPLAIN` 语句，但它的功能相对较基础。它可以用来显示查询的执行计划。
   - 用法示例：`EXPLAIN SELECT * FROM table;`
2. **EXPLAIN ESTIMATES**:
   - 这个命令提供了关于查询中各个阶段将要处理的行数和字节数的估计。
   - 用法示例：`EXPLAIN ESTIMATES SELECT * FROM table;`
3. **系统表**:
   - ClickHouse 提供了一些系统表，如 `system.query_log` 和 `system.query_thread_log`，可以用来分析查询的执行详情。
   - 通过查询这些表，可以获得关于执行时间、读取的行数、字节数等详细信息。
4. **PROFILE**:
   - `SET profile = 1;` 命令可以用来开启更详细的查询分析。
   - 执行此命令后，查询的结果将包含一个额外的 `ProfileEvents` 列，其中包含了各种性能指标，如 CPU 使用时间、内存使用量等。
5. **TRACE**:
   - `SET send_logs_level = 'trace';` 命令可以用来在查询执行时收集更多的日志信息。
   - 这可以帮助您了解查询执行的更多细节。
6. **FORMAT JSON**:
   - 将查询结果以 JSON 格式输出，可以获得关于查询执行的额外信息。
   - 用法示例：`SELECT * FROM table FORMAT JSON;`

使用这些工具，您可以深入了解 ClickHouse 中的查询执行情况，从而对查询进行优化和调整。不过，需要注意的是，这些工具在不同版本的 ClickHouse 中可能有所不同，因此最好参考您所使用的 ClickHouse 版本的官方文档。

### ✅ 数据字典(略)

《ClickHouse原理解析与应用实践》—— 第5章

### ✅ ❗️表引擎MergeTree & 索引 & 数据分区

在生产环境的绝大部分场景中，都会使用此系列的表引擎。因为只有合并树系列的表引擎才支持主键索引、数据分区、数据副本和数据采样这些特性，同时也只有此系列的表引擎支持ALTER相关操作。

《ClickHouse原理解析与应用实践》—— **第6章、第7章、第8章**

#### 🍊 分区经验(ChatGPT)

在 ClickHouse 中使用分区（Partitioning）可以带来许多优势，尤其是在处理大量数据时。以下是一些关于 ClickHouse 分区的使用经验和最佳实践：

1. **合理选择分区键**：分区键的选择对查询性能有很大影响。常见的做法是按时间分区，例如按月或按天分区。选择分区键时，要考虑查询模式，确保经常一起查询的数据位于同一分区内。
2. **避免过大或过小的分区**：分区既不应过大，也不应过小。过大的分区会影响数据的管理和维护，而过小的分区可能会增加查询时的开销。适当的分区大小依赖于数据的具体情况和查询需求。
3. **定期清理旧分区**：对于基于时间的分区，可以定期删除旧数据来释放空间。ClickHouse 支持高效的分区删除操作。
4. **利用分区进行高效的数据插入和更新**：在插入数据时，尽量将数据批量插入到相同的分区中，这样可以提高插入效率。同时，对于更新和删除操作，分区也可以提高效率。
5. **监控和优化分区策略**：随着数据的增长和查询模式的变化，原有的分区策略可能需要调整。定期监控分区的性能并根据需要进行调整是非常重要的。
6. **考虑分区与索引的结合使用**：在某些情况下，分区与索引的结合使用可以进一步优化查询性能。特别是在大表上执行范围查询时，适当的索引可以显著提高效率。
7. **适当的分区合并策略**：ClickHouse 允许手动或自动合并分区。适当的合并策略可以减少分区数量，优化查询效率。
8. **测试和评估**：在实施分区策略之前，最好在类似生产环境的测试环境中进行测试，以评估其对性能的影响。

每个应用场景都有其特定的需求和限制，因此在实际操作中，需要根据具体情况来调整分区策略。

### ✅ 副本与分片

《ClickHouse原理解析与应用实践》—— **第10章**

### ✅ 管理与运维

《ClickHouse原理解析与应用实践》—— **第11章**

